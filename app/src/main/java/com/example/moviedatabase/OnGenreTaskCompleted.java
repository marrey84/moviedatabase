package com.example.moviedatabase;

import java.util.List;

public interface OnGenreTaskCompleted {
    void onGenreTaskCompleted(List<Genre> genres);
}
