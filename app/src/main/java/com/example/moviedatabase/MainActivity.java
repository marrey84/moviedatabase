package com.example.moviedatabase;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    FragmentManager fragmentManager;
    SectionPageAdapter pageAdapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    public static DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();
        databaseHelper = new DatabaseHelper(this);
        fragmentManager = getSupportFragmentManager();
        pageAdapter = new SectionPageAdapter(getSupportFragmentManager());
        pageAdapter.addFragment(new TopRatedMovieFragment(),"Top rated");
        pageAdapter.addFragment(new MostPopularMovieFragment(),"Most Popular");
        viewPager.setAdapter(pageAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void findViews() {
        viewPager = findViewById( R.id.viewPager );
        tabLayout = findViewById(R.id.tabLayout);
    }


}
