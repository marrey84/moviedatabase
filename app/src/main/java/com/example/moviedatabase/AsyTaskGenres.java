package com.example.moviedatabase;

import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AsyTaskGenres extends AsyncTask<String,Void,List<Genre>> {

    private OnGenreTaskCompleted onGenresTaskCompletedListener;

    AsyTaskGenres(OnGenreTaskCompleted onGenresTaskCompletedListener)
    {
        this.onGenresTaskCompletedListener = onGenresTaskCompletedListener;
    }
    @Override
    protected List<Genre> doInBackground(String... strings) {
        URL url = null;
        try {
            url = new URL(strings[0]);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection conn = null;
        if (url != null) {
            try {
                conn = (HttpURLConnection) url.openConnection();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        if (conn != null) {
            try {
                conn.setRequestMethod("GET");
            } catch (ProtocolException e1) {
                e1.printStackTrace();
            }
        }
        InputStream in = null;
        if (conn != null) {
            try {
                in = new BufferedInputStream(conn.getInputStream());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        byte[] response = new byte[0];
        if (in != null) {
            try {
                response = org.apache.commons.io.IOUtils.toByteArray(in);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        String[] responseString = {""};
        try {
            responseString[0] = new String(response, 0, response.length, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        JSONArray genres = new JSONArray();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(responseString[0]);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            genres = jsonObject.getJSONArray("genres");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<Genre> generesList = new ArrayList<>();
        for (int i=0; i<genres.length(); i++)
        {
            try {
                Gson gson = new Gson();
                Genre genre = gson.fromJson(genres.get(i).toString(),Genre.class);
                generesList.add(genre);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return generesList;
    }

    @Override
    protected void onPostExecute(List<Genre> genres) {

        if (onGenresTaskCompletedListener != null) {
            onGenresTaskCompletedListener.onGenreTaskCompleted(genres);
        }
    }

}
