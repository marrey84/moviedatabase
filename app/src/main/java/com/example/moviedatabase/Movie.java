package com.example.moviedatabase;

import java.util.List;

public class Movie {

    long id;
    String title;
    List<Integer> genre_ids;
    String poster_path;
    float vote_average;
    String overview;

    public String getOverview() {
        return overview;
    }



    public Movie(long id, String title, List<Integer> genre_ids, String poster_path, float vote_average, String overview) {
        this.id = id;
        this.title = title;
        this.genre_ids = genre_ids;
        this.poster_path = poster_path;
        this.vote_average = vote_average;
        this.overview = overview;
    }

    public String getTitle() {
        return title;
    }

    public long getId() {
        return id;
    }

    public List<Integer> getGenre_ids() {
        return genre_ids;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public float getVote_average() {
        return vote_average;
    }




}
