package com.example.moviedatabase;

import android.content.ContentValues;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import static com.example.moviedatabase.MainActivity.databaseHelper;
import static com.example.moviedatabase.MovieFragment.apiKey;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    private Context context;
    private ArrayList<Movie> movieArrayList;
    MovieFragment movieFragment;
    Hashtable<Integer, String> genreHashtable = new Hashtable<>();
    private final String imageLink = "https://image.tmdb.org/t/p/w500/";
    private final String genresLink ="https://api.themoviedb.org/3/genre/movie/list?language=en-US&api_key="+apiKey;

    RecyclerViewAdapter(Context context, ArrayList<Movie> movieArrayList, MovieFragment movieFragment) {
        this.context = context;
        this.movieArrayList = movieArrayList;
        this.movieFragment = movieFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_movie,viewGroup,false);
        if(genreHashtable.isEmpty()) {
            if(movieFragment.checkConnection()) {
                new AsyTaskGenres(new OnGenreTaskCompleted() {
                    @Override
                    public void onGenreTaskCompleted(List<Genre> genres) {
                        for (int i = 0; i < genres.size(); i++) {
                            genreHashtable.put(genres.get(i).getId(), genres.get(i).getName());
                            ContentValues cv = new ContentValues();
                            cv.put(databaseHelper.colg_id, genres.get(i).getId());
                            cv.put(databaseHelper.colg_name, genres.get(i).getName());
                            databaseHelper.insertGenre(cv);
                        }
                        notifyDataSetChanged();

                    }
                }).execute(genresLink);
            }
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        final Movie movie = movieArrayList.get(position);
        viewHolder.title.setText(movie.getTitle());
        Picasso.get()
                .load(imageLink+movie.getPoster_path())
                .into(viewHolder.poster);
        viewHolder.rating.setText("Rating: "+movie.getVote_average());
        if(!genreHashtable.isEmpty())
        {
            String ganre = "";
            for (int i = 0; i < movie.getGenre_ids().size(); i++) {
                ganre += genreHashtable.get(movie.getGenre_ids().get(i)) + ", ";
            }
            ganre = ganre.substring(0, ganre.length() - 2);
            viewHolder.ganre.setText(ganre);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movieFragment.callDetails(movie.getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        if(movieArrayList != null)
            return movieArrayList.size();
        else
            return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView title;
        ImageView poster;
        TextView rating;
        TextView ganre;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            poster = itemView.findViewById(R.id.poster);
            rating = itemView.findViewById(R.id.rating);
            ganre = itemView.findViewById(R.id.ganre);
        }
    }
}
