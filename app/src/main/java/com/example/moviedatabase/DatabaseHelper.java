package com.example.moviedatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHelper extends SQLiteOpenHelper {
    private SQLiteDatabase db;
    private static final String dbName = "DB_Movies";
    private final String table_movies = "tb_movies";
    private final String table_ganres = "tb_ganres";


    final String colg_id = "colg_id";
    final String colg_name = "colg_name";

    final String colm_id = "colm_id";
    final String colm_title = "colm_title";
    final String colm_genre_ids = "colm_genre_ids";
    final String colm_poster_path = "colm_poster_path";
    final String colm_vote_average = "colm_vote_average";
    final String colm_overview = "colm_overview";

    DatabaseHelper(Context context) {
        super(context, dbName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTableGanres(db);
        createTableMovies(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //
        db.execSQL("DROP TABLE IF EXISTS "+table_ganres);
        db.execSQL("DROP TABLE IF EXISTS "+table_movies);
        onCreate(db);
    }

    private void createTableGanres(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE " +table_ganres + " ( "
                +colm_id + " INTEGER PRIMARY KEY,"
                +colm_title + " TEXT, "
                +colm_genre_ids + " TEXT, "
                +colm_poster_path + " TEXT, "
                +colm_vote_average + " REAL, "
                +colm_overview + " TEXT "
                + ");"
        );
    }

    private void createTableMovies(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE " +table_movies + " ( "
                +colg_id + " INTEGER PRIMARY KEY,"
                +colg_name + " TEXT "
                + ");"
        );
    }

    void insertMovie(ContentValues cv)
    {
        db = this.getWritableDatabase();
        db.insert(table_movies,null,cv);
    }

    void insertGenre(ContentValues cv)
    {
        db = this.getWritableDatabase();
        db.insert(table_ganres,null,cv);
    }

    Cursor selectAllGenres()
    {
        db = this.getReadableDatabase();
        return db.rawQuery("SELECT * FROM "+table_ganres , null);
    }

    Cursor selectAllMovies()
    {
        db = this.getReadableDatabase();
        return db.rawQuery("SELECT * FROM "+table_movies , null);
    }

    Cursor selectMovie(int id)
    {
        db = this.getReadableDatabase();
        return db.rawQuery("SELECT * FROM "+table_movies+" WHERE "+colm_id+" = '"+id+"'",null);
    }

    Cursor selectGanre(int id)
    {
        db = this.getReadableDatabase();
        return db.rawQuery("SELECT * FROM "+table_ganres+" WHERE "+colg_id+" = '"+id+"'",null);
    }
}
