package com.example.moviedatabase;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class TopRatedMovieFragment extends MovieFragment {


    public TopRatedMovieFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        link = "https://api.themoviedb.org/3/movie/top_rated?api_key="+apiKey+"&language=en-US&page=";
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    protected void findViews(View rootView) {
        super.findViews(rootView);
    }

}
