package com.example.moviedatabase;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import java.util.ArrayList;
import java.util.List;

public class AsyTaskMovies  extends AsyncTask<String,Integer,List<Movie>> {

    private OnMoviesTaskCompleted onMoviesTaskCompletedListener;
    private Dialog dialog;
    private Context context;
    private ProgressBar progressBar;
    private int total_pages;
    private int totalResults;

    public AsyTaskMovies(OnMoviesTaskCompleted onMoviesTaskCompletedListener, Context context) {
        this.onMoviesTaskCompletedListener = onMoviesTaskCompletedListener;
        this.context = context;
    }

    @Override
    protected List<Movie> doInBackground(String... strings) {
        URL url = null;
        try {
            url = new URL(strings[0]);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection conn = null;
        if (url != null) {
            try {
                conn = (HttpURLConnection) url.openConnection();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        if (conn != null) {
            try {
                conn.setRequestMethod("GET");
            } catch (ProtocolException e1) {
                e1.printStackTrace();
            }
        }
        InputStream in = null;
        if (conn != null) {
            try {
                in = new BufferedInputStream(conn.getInputStream());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        byte[] response = new byte[0];
        if (in != null) {
            try {
                response = org.apache.commons.io.IOUtils.toByteArray(in);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        String[] responseString = {""};
        try {
            responseString[0] = new String(response, 0, response.length, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        JSONArray movies = new JSONArray();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(responseString[0]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            total_pages = jsonObject.getInt("total_pages");
            totalResults = jsonObject.getInt("total_results");
            movies = jsonObject.getJSONArray("results");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        List<Movie> movieList = new ArrayList<>();
        for (int i=0; i<movies.length(); i++)
        {
            try {
                Gson gson = new Gson();
                Movie movie = gson.fromJson(movies.get(i).toString(),Movie.class);
                movieList.add(movie);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return movieList;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new Dialog(context, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.progress_bar_dialog, null, false);
        dialog.setContentView(dialogView);

        progressBar = dialogView.findViewById(R.id.progressBar);
        progressBar.setProgress(0);
        dialog.show();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        progressBar.setMax(values[1]);
        progressBar.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(List<Movie> movies) {

        if (onMoviesTaskCompletedListener != null) {
            onMoviesTaskCompletedListener.onMoviesTaskCompleted(movies,total_pages,totalResults);
        }
        dialog.cancel();
    }
}
