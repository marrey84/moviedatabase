package com.example.moviedatabase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MovieDetails extends AppCompatActivity {

    private ImageView poster;
    private TextView title;
    private TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        findViews();
        String id = getIntent().getStringExtra("id");
        Toast.makeText(this,id+"",Toast.LENGTH_SHORT).show();

    }

    private void findViews() {
        poster = findViewById( R.id.poster );
        title = findViewById( R.id.title );
        description = findViewById( R.id.description );
    }
}
