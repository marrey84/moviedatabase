package com.example.moviedatabase;


import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.example.moviedatabase.MainActivity.databaseHelper;


/**
 * A simple {@link Fragment} subclass.
 */
public class MovieFragment extends Fragment {

    protected static final String apiKey = "c22d755514350d9836b3f9b173b3d763";
    protected int currentPage=1;
    ArrayList<Movie> movieArrayList = new ArrayList<>();;
    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerViewAdapter;
    protected String link;
    private Button buttonPrev;
    private Button buttonNext;
    private TextView pageInfo;
    public MovieFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_movie, container, false);
        findViews(rootView);
        loadMovies(link);
        recyclerViewAdapter = new RecyclerViewAdapter(getActivity(), movieArrayList, this);
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),1));
        return rootView;
    }

    protected void findViews(View rootView) {
        recyclerView = rootView.findViewById(R.id.recyclerView);
        buttonPrev = rootView.findViewById( R.id.buttonPrev );
        buttonNext = rootView.findViewById( R.id.buttonNext );
        pageInfo = rootView.findViewById(R.id.pageInfo);
        buttonNext.setOnClickListener(clickListener);
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if ( v == buttonPrev ) {
                currentPage--;
            } else if ( v == buttonNext ) {
                currentPage ++;

            }
            loadMovies(link);
        }
    };

    protected void loadMovies(String link)
    {
        if(checkConnection()) {
            link = link + currentPage;
            if (movieArrayList.size() > 0)
                movieArrayList.clear();
            new AsyTaskMovies(new OnMoviesTaskCompleted() {
                @Override
                public void onMoviesTaskCompleted(List<Movie> movies, int totalResults, int total_Pages) {
                    movieArrayList.addAll(movies);
                    addMoviesInDataBase();
                    recyclerViewAdapter.notifyDataSetChanged();
                    if (currentPage == 1)
                        buttonPrev.setEnabled(false);
                    else
                        buttonPrev.setEnabled(true);
                    if (currentPage == total_Pages)
                        buttonNext.setEnabled(false);
                    else
                        buttonNext.setEnabled(true);
                    pageInfo.setText(currentPage + ". page of " + total_Pages + " pages");
                }
            }, getContext()).execute(link);
        }
    }

    private void addMoviesInDataBase()
    {
        for(int i=0; i<movieArrayList.size(); i++) {
            ContentValues cv = new ContentValues();
            cv.put(databaseHelper.colm_id, movieArrayList.get(i).getId());
            cv.put(databaseHelper.colm_title, movieArrayList.get(i).getTitle());
            String genresIds = "";

            for(int j=0; j<movieArrayList.get(i).getGenre_ids().size(); j++)
                genresIds += movieArrayList.get(i).getGenre_ids().get(j)+",";
            genresIds = genresIds.substring(0, genresIds.length() - 2);
            cv.put(databaseHelper.colm_genre_ids, genresIds);
            cv.put(databaseHelper.colm_poster_path, movieArrayList.get(i).getPoster_path());
            cv.put(databaseHelper.colm_vote_average, movieArrayList.get(i).getVote_average());
            cv.put(databaseHelper.colm_overview, movieArrayList.get(i).getOverview());
            databaseHelper.insertMovie(cv);
        }

    }

    public void callDetails(long id)
    {
        Intent intent = new Intent(getContext(), MovieDetails.class);
        intent.putExtra("id",id);
        startActivity( intent);
    }

    public boolean checkConnection() {
        String command = "ping -c 1 google.com";
        try {
            if ((Runtime.getRuntime().exec (command).waitFor() == 0))
                return true;
            else
            {
                Toast.makeText(getContext(), "No Internet",Toast.LENGTH_LONG).show();
                return false;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(getContext(), "No Internet",Toast.LENGTH_LONG).show();
        return false;
    }
}
