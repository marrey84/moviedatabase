package com.example.moviedatabase;
import java.util.List;

public interface OnMoviesTaskCompleted {
    void onMoviesTaskCompleted(List<Movie> movies, int totalResults, int total_Pages);
}
